'use strict';
var Foxx = require('org/arangodb/foxx');
var joi = require('joi');

class FlashcardSetRevisionModel extends Foxx.Model {
  static getJoiSchema() {
    return {
      _key: joi.string(),
      _rev: joi.string(),
      _id: joi.string(),
      user: joi.string().required(),
      flashcardSet: joi.string().required(),
      flashcardSetRevision: joi.array().items(joi.object().keys({
        title: joi.string(),
        description: joi.string(),
        swapQuestionsWithAnswersRandomly: joi.boolean(),
        flashcards: joi.array().items(joi.object().keys({
          question: joi.string().required(),
          questionImageUrl: joi.string().uri({ scheme: ['https', 'http'] }),
          answer: joi.string().required(),
          answerImageUrl: joi.string().uri({ scheme: ['https', 'http'] })
        }))
      })),
      createdAt: joi.date().iso().default(new Date())
    }
  }
}

FlashcardSetRevisionModel.prototype.schema = joi.object().keys(FlashcardSetRevisionModel.getJoiSchema());

exports.Model = FlashcardSetRevisionModel;
