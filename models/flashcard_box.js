'use strict';
var Foxx = require('org/arangodb/foxx');
var joi = require('joi');

class FlashcardBoxModel extends Foxx.Model {

  getNextDefaultFlashcardId() {
    return this.getNextFlashcardId(1);
  }

  getNextFlashcardId(compartmentIndex) {
    // compartmentIndex has to be 1, 2, 3 or 4
    var flashcardBoxCompartments = this.get('flashcardBoxCompartments');
    var i;
    for (i = 0; i < flashcardBoxCompartments.length - 1; i++) {
      if (flashcardBoxCompartments[i].compartmentSize == flashcardBoxCompartments[i].flashcards.length) {
        // Return flaashcard of full compartment
        return flashcardBoxCompartments[i].flashcards[0];
      }
    }

    var arr = [1, 0, 2, 3, 4];

    if (flashcardBoxCompartments[compartmentIndex].flashcards.length > 0) {
      return flashcardBoxCompartments[compartmentIndex].flashcards[0];
    }  

    for (var i in arr) {
      var currentCompartmentIndex = arr[i];

      if (flashcardBoxCompartments[currentCompartmentIndex].flashcards.length > 0) {
        return flashcardBoxCompartments[currentCompartmentIndex].flashcards[0];
      } else if (currentCompartmentIndex == 1 && flashcardBoxCompartments[0].flashcards.length > 0) {
        // Move Flashcards from compartent 0 to compartment 1
        if (flashcardBoxCompartments[0].flashcards.length >= flashcardBoxCompartments[1].compartmentSize) {
          flashcardBoxCompartments[1].flashcards = flashcardBoxCompartments[1].flashcards.concat(flashcardBoxCompartments[0].flashcards.slice(0, flashcardBoxCompartments[1].compartmentSize));
          flashcardBoxCompartments[0].flashcards = flashcardBoxCompartments[0].flashcards.slice(flashcardBoxCompartments[1].compartmentSize, flashcardBoxCompartments[0].flashcards.length);
        } else {
          flashcardBoxCompartments[1].flashcards = flashcardBoxCompartments[1].flashcards.concat(flashcardBoxCompartments[0].flashcards);
          flashcardBoxCompartments[0].flashcards = [];
        }
        return flashcardBoxCompartments[1].flashcards[0];
      }
    }

    return null;
  }

  isFinished() {
    var flashcardBoxCompartments = this.get('flashcardBoxCompartments');
    return flashcardBoxCompartments[0].flashcards.length === 0 && flashcardBoxCompartments[1].flashcards.length === 0 
      && flashcardBoxCompartments[2].flashcards.length === 0 && flashcardBoxCompartments[3].flashcards.length === 0
      && flashcardBoxCompartments[4].flashcards.length === 0
  }

  static getJoiSchema() {
    return {
      _key: joi.string(),
      _rev: joi.string(),
      _id: joi.string(),
      user: joi.string().required(),
      flashcardSet: joi.string().required(),
      flashcardBoxCompartments: joi.array().items(joi.object().keys({
        compartmentSize: joi.number().min(1).integer().required(),
        flashcards: joi.array().items(joi.string())
      })),
      createdAt: joi.date().iso().default(new Date()),
      updatedAt: joi.date().iso().default(new Date())
    }
  }
}

FlashcardBoxModel.prototype.schema = joi.object().keys(FlashcardBoxModel.getJoiSchema());

exports.Model = FlashcardBoxModel;