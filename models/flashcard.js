'use strict';
var Foxx = require('org/arangodb/foxx');
var joi = require('joi');

class FlashcardModel extends Foxx.Model {

  swapQuestionWithAnswer() {
    var question = this.get('question');
    var questionImageUrl = this.get('questionImageUrl');
    var answer = this.get('answer');
    var answerImageUrl = this.get('answerImageUrl');
    this.set('question', answer);
    this.set('questionImageUrl', answerImageUrl);
    this.set('answer', question);
    this.set('answerImageUrl', questionImageUrl);
  }

  static getJoiSchema() {
    return {
      _key: joi.string(),
      _rev: joi.string(),
      _id: joi.string(),
      question: joi.string().min(3).max(10000).required(),
      questionImageUrl: joi.string().uri({ scheme: ['https', 'http'] }),
      answer: joi.string().min(3).max(10000).required(),
      answerImageUrl: joi.string().uri({ scheme: ['https', 'http'] }),
      flashcardSet: joi.string(),
      createdAt: joi.date().iso().default(new Date()),
      updatedAt: joi.date().iso().default(new Date())
    };
  }
}

FlashcardModel.prototype.schema = joi.object().keys(FlashcardModel.getJoiSchema());

exports.Model = FlashcardModel;