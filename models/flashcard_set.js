'use strict';
var Foxx = require('org/arangodb/foxx');
var joi = require('joi');

class FlashcardSetModel extends Foxx.Model {
  static getJoiSchema() {
    return {
      // Describe the attributes with joi here
      _key: joi.string(),
      _rev: joi.string(),
      _id: joi.string(),
      title: joi.string().min(3).max(30).required(),
      description: joi.string().max(300),
      swapQuestionsWithAnswersRandomly: joi.boolean().required(),
      createdAt: joi.date().iso().default(new Date()),
      updatedAt: joi.date().iso().default(new Date())
    };
  }
}

FlashcardSetModel.prototype.schema = joi.object().keys(FlashcardSetModel.getJoiSchema());

exports.Model = FlashcardSetModel;