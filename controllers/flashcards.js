'use strict';
var _ = require('underscore');
var joi = require('joi');
var Foxx = require('org/arangodb/foxx');
var ArangoError = require('org/arangodb').ArangoError;
var db = require('org/arangodb').db;
var FlashcardsRepository = require('../repositories/flashcards').Repository;
var FlashcardModel = require('../models/flashcard').Model;
var FlashcardSetsRepository = require('../repositories/flashcard_sets').Repository;
var FlashcardSetModel = require('../models/flashcard_set').Model;
var FlashcardBoxesRepository = require('../repositories/flashcard_boxes').Repository;
var FlashcardBoxModel = require('../models/flashcard_box').Model;
var console = require('console')

var controller = new Foxx.Controller(applicationContext);

var sessionStorage = applicationContext.dependencies.sessions.sessionStorage

controller.activateSessions({
  sessionStorage: sessionStorage,
  header: 'Authorization'
});

var flashcardIdSchema = joi.string().required()
  .description('The id of the Flashcard')
  .meta({
    allowMultiple: false
  });

var flashcardsRepository = new FlashcardsRepository(
  applicationContext.collection('Flashcards'), {
    model: FlashcardModel
  }
);

var flashcardSetsRepository = new FlashcardSetsRepository(
  applicationContext.collection('FlashcardSets'), {
    model: FlashcardSetModel
  }
);

var flashcardBoxesRepository = new FlashcardBoxesRepository(
  applicationContext.collection('FlashcardBoxes'), {
    model: FlashcardBoxModel
  }
);

var getFlashcardBoxCompartmentIndex = function(flashcardBoxCompartments, flashcardId) {
  return _.findIndex(flashcardBoxCompartments, function(compartment) {
    return _.contains(compartment.flashcards, flashcardId);
  });
}

var removeFlashcardFromFlashcardBoxCompartment = function(flashcards, flashcardId) {
  return _.reject(flashcards,
    function(internalFlashcardId) {
      return internalFlashcardId == flashcardId;
    });
}

controller.before(function(req, res) {
  var userId = req.session.attributes.uid;
  if (userId != null) {
    var userEmail = req.session.attributes.sessionData.email;
    console.log("USERLOG url: %s %s userId: %s userEmail: %s ", req.requestType, req.url, userId, userEmail);
  }
});

/** Called if the Flashcard was known.
 *
 * Called if the Flashcard was known. The flashcard will be moved to the next compartment and a new flashcard from will be returned.
 */
controller.get('/:id/known', function(req, res) {
    var userId = req.session.attributes.uid;
    var id = req.urlParameters.id;
    db._executeTransaction({
      collections: {
        read: [
          applicationContext.collectionName('Flashcards')
        ],
        write: [
          applicationContext.collectionName('FlashcardBoxes')
        ]
      },
      action: function() {
        var flashcard = flashcardsRepository.byId(id);
        var flashcardSetId = flashcard.get('flashcardSet');
        var flashcardSet = flashcardSetsRepository.byId(flashcardSetId);
        var flashcardBox = flashcardBoxesRepository.firstExample({
          user: userId,
          flashcardSet: flashcardSetId
        });

        var flashcardBoxCompartments = flashcardBox.get('flashcardBoxCompartments');
        var flashcardId = flashcard.get('_id');
        var flashcardBoxCompartmentIndex = getFlashcardBoxCompartmentIndex(flashcardBoxCompartments, flashcardId);

        if (flashcardBoxCompartmentIndex > 0 && flashcardBoxCompartmentIndex < 5) {
          // Remove flashcard from current compartment
          flashcardBoxCompartments[flashcardBoxCompartmentIndex].flashcards = removeFlashcardFromFlashcardBoxCompartment(flashcardBoxCompartments[flashcardBoxCompartmentIndex].flashcards, flashcardId);

          // Add flashcard to next compartment
          flashcardBoxCompartments[flashcardBoxCompartmentIndex + 1].flashcards.push(flashcardId);
        }

        var nextFlashcardId = flashcardBox.getNextFlashcardId(flashcardBoxCompartmentIndex);
        flashcardBoxesRepository.replace(flashcardBox);
        if (nextFlashcardId === null) {
          res.status(204);
        } else {
          flashcardBoxCompartmentIndex = getFlashcardBoxCompartmentIndex(flashcardBoxCompartments, nextFlashcardId);
          var nextFlashcard = flashcardsRepository.byId(nextFlashcardId);
          if (Math.round(Math.random()) == 1 && flashcardSet.get('swapQuestionsWithAnswersRandomly')) {
            nextFlashcard.swapQuestionWithAnswer()
          }
          var result = nextFlashcard.forClient();
          result.compartmentIndex = flashcardBoxCompartmentIndex;
          res.json(result);
        }
      }
    });
  })
  .onlyIfAuthenticated(401, 'You need to be authenticated')
  .pathParam('id', flashcardIdSchema)
  .errorResponse(ArangoError, 404, 'The Flashcard could not be found');

/** Called if the Flashcard was not known.
 *
 * Called if the Flashcard was not known. The flashcard will be moved to the previous compartment and a new flashcard from will be returned.
 */
controller.get('/:id/unknown', function(req, res) {
    var userId = req.session.attributes.uid;
    var id = req.urlParameters.id;
    db._executeTransaction({
      collections: {
        read: [
          applicationContext.collectionName('Flashcards')
        ],
        write: [
          applicationContext.collectionName('FlashcardBoxes')
        ]
      },
      action: function() {
        var flashcard = flashcardsRepository.byId(id);
        var flashcardSetId = flashcard.get('flashcardSet');
        var flashcardSet = flashcardSetsRepository.byId(flashcardSetId);
        var flashcardBox = flashcardBoxesRepository.firstExample({
          user: userId,
          flashcardSet: flashcardSetId
        });

        var flashcardBoxCompartments = flashcardBox.get('flashcardBoxCompartments');
        var flashcardId = flashcard.get('_id');
        var flashcardBoxCompartmentIndex = getFlashcardBoxCompartmentIndex(flashcardBoxCompartments, flashcardId);

        if (flashcardBoxCompartmentIndex > 0) {
          // Remove flashcard from current compartment
          flashcardBoxCompartments[flashcardBoxCompartmentIndex].flashcards = removeFlashcardFromFlashcardBoxCompartment(flashcardBoxCompartments[flashcardBoxCompartmentIndex].flashcards, flashcardId);

          // Add flashcard to first compartment
          flashcardBoxCompartments[1].flashcards.push(flashcardId);
        }

        var nextFlashcardId = flashcardBox.getNextFlashcardId(flashcardBoxCompartmentIndex);
        flashcardBoxesRepository.replace(flashcardBox);
        if (nextFlashcardId === null) {
          res.status(204);
        } else {
          flashcardBoxCompartmentIndex = getFlashcardBoxCompartmentIndex(flashcardBoxCompartments, nextFlashcardId);
          var nextFlashcard = flashcardsRepository.byId(nextFlashcardId);
          if (Math.round(Math.random()) == 1 && flashcardSet.get('swapQuestionsWithAnswersRandomly')) {
            nextFlashcard.swapQuestionWithAnswer()
          }
          var result = nextFlashcard.forClient();
          result.compartmentIndex = flashcardBoxCompartmentIndex;
          res.json(result);
        }
      }
    });
  })
  .onlyIfAuthenticated(401, 'You need to be authenticated')
  .pathParam('id', flashcardIdSchema)
  .errorResponse(ArangoError, 404, 'The Flashcard could not be found');