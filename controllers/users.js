'use strict';
var _ = require('underscore');
var joi = require('joi');
var Foxx = require('org/arangodb/foxx');
var ArangoError = require('org/arangodb').ArangoError;
var Auth = require('../util/auth').Auth;
var NotLoggedIn = require('../util/auth').NotLoggedIn;
var console = require('console');

var controller = new Foxx.Controller(applicationContext);

var sessionStorage = applicationContext.dependencies.sessions.sessionStorage

controller.activateSessions({
  sessionStorage: sessionStorage,
  header: 'Authorization'
});

controller.before(function(req, res) {
  var userId = req.session.attributes.uid;
  if (userId != null) {
    var userEmail = req.session.attributes.sessionData.email;
    console.log("USERLOG url: %s %s userId: %s userEmail: %s ", req.requestType, req.url, userId, userEmail);
  }
});

/** Register a new user
 *
 * Add a new user to the system with a given email and password
 */
controller.post('/4e00b455b2b347f8bf67-register-560ab5b03b4d', function(req, res) {
  var credentials = req.params('credentials');
  var user = Auth.createUser(credentials);

  res.status(201);
  res.json({
    email: user.get('user')
  });
}).bodyParam('credentials', {
  description: 'Email and Password',
  type: joi.object().keys({
    email: joi.string().required(),
    password: joi.string().required()
  })
});

/** Login an user
 *
 * An user is logged in to the system with a given email and password
 */
controller.post('/login', function(req, res) {
    var credentials = req.params('credentials');
    var session = sessionStorage.create();
    var user = Auth.login(credentials, session);

    res.status(201);
    res.json({
      token: session.forClient()
    });
  }).bodyParam('credentials', {
    description: 'Email and Password',
    type: joi.object().keys({
      email: joi.string().required(),
      password: joi.string().required()
    })
  })
  .errorResponse(NotLoggedIn, 401, 'Invalid email or password');