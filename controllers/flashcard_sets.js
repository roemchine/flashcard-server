'use strict';
var _ = require('underscore');
var joi = require('joi');
var Foxx = require('org/arangodb/foxx');
var ArangoError = require('org/arangodb').ArangoError;
var db = require('org/arangodb').db;
var FlashcardsRepository = require('../repositories/flashcards').Repository;
var FlashcardModel = require('../models/flashcard').Model;
var FlashcardSetsRepository = require('../repositories/flashcard_sets').Repository;
var FlashcardSetModel = require('../models/flashcard_set').Model;
var FlashcardSetRevisionsRepository = require('../repositories/flashcard_set_revisions').Repository;
var FlashcardSetRevisionModel = require('../models/flashcard_set_revision').Model;
var FlashcardBoxesRepository = require('../repositories/flashcard_boxes').Repository;
var FlashcardBoxModel = require('../models/flashcard_box').Model;
var console = require('console');

var controller = new Foxx.Controller(applicationContext);

var sessionStorage = applicationContext.dependencies.sessions.sessionStorage

controller.activateSessions({
  sessionStorage: sessionStorage,
  header: 'Authorization'
});

var flashcardSetIdSchema = joi.string().required()
  .description('The id of the FlashcardSet')
  .meta({
    allowMultiple: false
  });

var flashcardSetSaveSchema = (function() {
  var model = FlashcardSetModel.getJoiSchema();
  model.flashcards = joi.array().items(joi.object().keys(FlashcardModel.getJoiSchema()));
  return joi.object().keys(model);
})();

var OptimisticLockingError = function() {
  this.name = 'OptimisticLockingError';
};
OptimisticLockingError.prototype = new Error();

var flashcardSetsRepository = new FlashcardSetsRepository(
  applicationContext.collection('FlashcardSets'), {
    model: FlashcardSetModel
  }
);

var flashcardSetRevisionsRepository = new FlashcardSetRevisionsRepository(
  applicationContext.collection('FlashcardSetRevisions'), {
    model: FlashcardSetRevisionModel
  }
);

var flashcardsRepository = new FlashcardsRepository(
  applicationContext.collection('Flashcards'), {
    model: FlashcardModel
  }
);

var flashcardBoxesRepository = new FlashcardBoxesRepository(
  applicationContext.collection('FlashcardBoxes'), {
    model: FlashcardBoxModel
  }
);

controller.before(function(req, res) {
  var userId = req.session.attributes.uid;
  if (userId != null) {
    var userEmail = req.session.attributes.sessionData.email;
    console.log("USERLOG url: %s %s userId: %s userEmail: %s ", req.requestType, req.url, userId, userEmail);
  }
});

/** Lists of all FlashcardSets.
 *
 * This function simply returns the list of all FlashcardSet.
 */
controller.get('/', function(req, res) {
  var flashcardSetsForClient = _.map(flashcardSetsRepository.getAllFlashcardSets(req.parameters.skip, req.parameters.limit), function(model) {
    return model.forClient();
  });
  var totalLengthOfFlashcardSets = flashcardSetsRepository.getFlashcardSetsLength();
  res.json({
    flashcardSets: flashcardSetsForClient,
    totalLengthOfFlashcardSets: totalLengthOfFlashcardSets
  });
}).queryParam('skip', {
  type: joi.number().integer(),
  required: true,
  description: 'Numer of records to skip'
}).queryParam('limit', {
  type: joi.number().integer(),
  required: true,
  description: 'Numer of records to get'
});

/** Creates a new FlashcardSet.
 *
 * Creates a new FlashcardSet. The information has to be in the
 * requestBody.
 */
controller.post('/', function(req, res) {
    var userId = req.session.attributes.uid;
    var flashcardSet = req.parameters.FlashcardSet;
    var response = {};
    try {
      db._executeTransaction({
        collections: {
          write: [
            applicationContext.collectionName('FlashcardSets'),
            applicationContext.collectionName('FlashcardSetRevisions'),
            applicationContext.collectionName('Flashcards')
          ]
        },
        action: function() {
          flashcardSet.createdAt = new Date();
          flashcardSet.updatedAt = new Date();
          var flashcardSetSaveResult = flashcardSetsRepository.save(_.omit(flashcardSet, 'flashcards'));
          response = flashcardSetSaveResult.forClient();

          response.flashcards = [];
          var flashcardArray = _.pick(flashcardSet, 'flashcards').flashcards;
          _.each(flashcardArray, function(flashcard) {
            var sanitizedFlashcard = _.pick(flashcard, '_key', 'question', 'questionImageUrl', 'answer', 'answerImageUrl');
            sanitizedFlashcard.createdAt = new Date();
            sanitizedFlashcard.updatedAt = new Date();
            sanitizedFlashcard.flashcardSet = flashcardSetSaveResult.attributes._id;
            response.flashcards.push(flashcardsRepository.save(sanitizedFlashcard).forClient());
          });
          flashcardSetRevisionsRepository.createRevision(flashcardSetSaveResult.get('_id'), userId);
        }
      });
    } catch (e) {
      res.json(e);
      console.log(e);
      return;
    }
    res.json(response);
  })
  .onlyIfAuthenticated(401, 'You need to be authenticated')
  .bodyParam('FlashcardSet', {
    description: 'The flashcard_set you want to create',
    type: flashcardSetSaveSchema
  });

/** Reads a FlashcardSet.
 *
 * Reads a FlashcardSet.
 */
controller.get('/:id', function(req, res) {
    var userId = req.session.attributes.uid;
    var id = req.urlParameters.id;
    var result = flashcardSetsRepository.getFlashcardSetWithFlashcardsAndCompartmentsForUser(id, userId)[0];
    res.json(result);
  })
  .onlyIfAuthenticated(401, 'You need to be authenticated')
  .pathParam('id', flashcardSetIdSchema)
  .errorResponse(ArangoError, 404, 'The FlashcardSet could not be found');

/** Returns revisions of a FlashcardSet
 *
 * Returns all revisions of a flashcard set
 */
controller.get('/:id/revisions', function(req, res) {
    var id = req.urlParameters.id;
    var flashcardSet = flashcardSetsRepository.byId(id);
    var revisionsForClient = _.map(flashcardSetRevisionsRepository.getRevisions(flashcardSet.get('_id'), req.parameters.skip, req.parameters.limit), function(model) {
      return model.forClient();
    });
    var revisionLength = flashcardSetRevisionsRepository.getRevisionsLength(flashcardSet.get('_id'));
    res.json({
      flashcardSetRevisions: revisionsForClient,
      totalLengthOfFlashcardSetRevisions: revisionLength
    });
  })
  .onlyIfAuthenticated(401, 'You need to be authenticated')
  .pathParam('id', flashcardSetIdSchema)
  .queryParam('skip', {
    type: joi.number().integer(),
    required: true,
    description: 'Numer of records to skip'
  }).queryParam('limit', {
    type: joi.number().integer(),
    required: true,
    description: 'Numer of records to get'
  })
  .errorResponse(ArangoError, 404, 'The FlashcardSet could not be found');

/** Start to learn a FlashcardSet.
 *
 * Start to learn a FlashcardSet. A FlashcardBox will be created if required. A Flashcard will be returned
 */
controller.get('/:id/learn', function(req, res) {
    var userId = req.session.attributes.uid;
    var id = req.urlParameters.id;
    db._executeTransaction({
      collections: {
        read: [applicationContext.collectionName('FlashcardSets'),
          applicationContext.collectionName('Flashcards')
        ],
        write: [
          applicationContext.collectionName('FlashcardBoxes')
        ]
      },
      action: function() {
        var storedFlashcardSet = flashcardSetsRepository.getFlashcardSetWithFlashcards(id)[0];
        var flashcardBox = flashcardBoxesRepository.firstExample({
          user: userId,
          flashcardSet: storedFlashcardSet._id
        });

        if (flashcardBox != null && flashcardBox.isFinished()) {
          flashcardBoxesRepository.remove(flashcardBox);
          flashcardBox = null;
        }

        if (flashcardBox === null) {
          var numberOfFlashcards = storedFlashcardSet.flashcards.length;
          var compartment5Size = numberOfFlashcards * 2;
          var compartment4Size = Math.round(compartment5Size / 1.5);
          var compartment3Size = Math.round(compartment4Size / 1.5);
          var compartment2Size = Math.round(compartment3Size / 1.5);
          var compartment1Size = Math.round(compartment2Size / 1.5);

          var compartment1Flashcards = storedFlashcardSet.flashcards.slice(0, compartment1Size)
          var compartment0Flashcards = storedFlashcardSet.flashcards.slice(compartment1Size, numberOfFlashcards);

          flashcardBox = new FlashcardBoxModel({
            user: userId,
            flashcardSet: storedFlashcardSet._id,
            flashcardBoxCompartments: [{
              compartmentSize: Number.POSITIVE_INFINITY,
              flashcards: _.pluck(compartment0Flashcards, '_id')
            }, {
              compartmentSize: compartment1Size,
              flashcards: _.pluck(compartment1Flashcards, '_id')
            }, {
              compartmentSize: compartment2Size,
              flashcards: []
            }, {
              compartmentSize: compartment3Size,
              flashcards: []
            }, {
              compartmentSize: compartment4Size,
              flashcards: []
            }, {
              compartmentSize: compartment5Size,
              flashcards: []
            }]
          });
          flashcardBoxesRepository.save(flashcardBox);
          res.json(compartment1Flashcards[0]);
        } else {
          var nextFlashcardId = flashcardBox.getNextDefaultFlashcardId();
          flashcardBoxesRepository.replace(flashcardBox);
          if (nextFlashcardId === null) {
            res.status(204);
          } else {
            var flashcardBoxCompartmentIndex = _.findIndex(flashcardBox.get('flashcardBoxCompartments'), function(compartment) {
              return _.contains(compartment.flashcards, nextFlashcardId);
            });
            var nextFlashcard = flashcardsRepository.byId(nextFlashcardId);
            if (Math.round(Math.random()) == 1 && storedFlashcardSet.swapQuestionsWithAnswersRandomly) {
              nextFlashcard.swapQuestionWithAnswer()
            }
            var result = nextFlashcard.forClient();
            result.compartmentIndex = flashcardBoxCompartmentIndex;
            res.json(result);
          }
        }
      }
    });
  })
  .onlyIfAuthenticated(401, 'You need to be authenticated')
  .pathParam('id', flashcardSetIdSchema)
  .errorResponse(ArangoError, 404, 'The Flashcard could not be found');

/** Replaces a FlashcardSet.
 *
 * Changes a FlashcardSet. The information has to be in the
 * requestBody.
 */
controller.put('/:id', function(req, res) {
    var userId = req.session.attributes.uid;
    var id = req.urlParameters.id;
    var flashcardSet = req.parameters.FlashcardSet;
    var response = {};
    db._executeTransaction({
      collections: {
        write: [
          applicationContext.collectionName('FlashcardSets'),
          applicationContext.collectionName('FlashcardSetRevisions'),
          applicationContext.collectionName('Flashcards'),
          applicationContext.collectionName('FlashcardBoxes')
        ]
      },
      action: function() {
        var storedFlashcardSet = flashcardSetsRepository.getFlashcardSetWithFlashcards(id)[0];
        if (new Date(flashcardSet.updatedAt).getUTCMilliseconds() !== new Date(storedFlashcardSet.updatedAt).getUTCMilliseconds()) {
          throw new OptimisticLockingError();
        }

        flashcardSet.createdAt = new Date(storedFlashcardSet.createdAt);
        flashcardSet.updatedAt = new Date();
        response = flashcardSetsRepository.replaceById(id, _.omit(flashcardSet, 'flashcards'));

        response.flashcards = [];
        var storedFlashcardsArray = _.pick(storedFlashcardSet, 'flashcards').flashcards;
        var flashcardsArray = _.pick(flashcardSet, 'flashcards').flashcards;
        var flashcardKeysToKeep = _.pluck(flashcardsArray, '_key');

        _.each(storedFlashcardsArray, function(flashcard) {
          if (!_.contains(flashcardKeysToKeep, flashcard._key)) {
            // Delete flashcard
            flashcardsRepository.removeById(flashcard._key);

            flashcardBoxesRepository.removeFlashcardFromFlashcardBox(response._id, flashcard._id);
          }
        });

        _.each(flashcardsArray, function(flashcard) {
          if (flashcard._key !== undefined && flashcard.flashcardSet === storedFlashcardSet._id) {

            var storedFlashcard = _.findWhere(storedFlashcardSet.flashcards, {
              _key: flashcard._key
            });
            if (storedFlashcard.question != flashcard.question || storedFlashcard.questionImageUrl != flashcard.questionImageUrl || storedFlashcard.answer != flashcard.answer || storedFlashcard.answerImageUrl != flashcard.answerImageUrl) {
              // Update flashcard
              flashcard.createdAt = storedFlashcard.createdAt;
              flashcard.updatedAt = new Date();
              flashcard.flashcardSet = response._id;
              response.flashcards.push(flashcardsRepository.replaceById(flashcard._key, flashcard));

              flashcardBoxesRepository.removeFlashcardFromFlashcardBox(response._id, flashcard._id);
              flashcardBoxesRepository.addNewFlashcardToFlashcardBoxes(response._id, flashcard._id);
            }

          } else if (!flashcardsRepository.exists(flashcard._key)) {
            // Create flashcard
            var sanitizedFlashcard = _.pick(flashcard, '_key', 'question', 'questionImageUrl', 'answer', 'answerImageUrl');
            sanitizedFlashcard.createdAt = new Date();
            sanitizedFlashcard.updatedAt = new Date();
            sanitizedFlashcard.flashcardSet = storedFlashcardSet._id;
            var savedFlashcard = flashcardsRepository.save(sanitizedFlashcard).forClient();
            response.flashcards.push(savedFlashcard);

            flashcardBoxesRepository.addNewFlashcardToFlashcardBoxes(response._id, savedFlashcard._id);
          } else {
            // TODO throw error
          }
        });
        flashcardSetRevisionsRepository.createRevision(response._id, userId);
      }
    });

    res.json(response);
  })
  .onlyIfAuthenticated(401, 'You need to be authenticated')
  .pathParam('id', flashcardSetIdSchema)
  .bodyParam('FlashcardSet', {
    description: 'The FlashcardSet you want your old one to be replaced with',
    type: flashcardSetSaveSchema
  })
  .errorResponse(ArangoError, 404, 'The FlashcardSet could not be found')
  .errorResponse(OptimisticLockingError, 422, 'The FlashcardSet has changed meanwhile');
