'use strict';
var _ = require('underscore'),
  Foxx = require('org/arangodb/foxx'),
  auth = Foxx.requireApp('/_system/simple-auth').auth,
  users = Foxx.requireApp('/_system/users').userStorage;

class NotLoggedIn extends Error {

}

class Auth {
  static createUser(credentials) {
    var password = auth.hashPassword(credentials.password);
    return users.create(credentials.email, {}, {
      simple: password
    });
  }

  static login(credentials, session) {
    var user = users.resolve(credentials.email);
    var valid = auth.verifyPassword(
      user ? user.get('authData').simple : {},
      credentials.password
    );

    if (!valid) {
      throw new NotLoggedIn();
    }

    session.get('sessionData').email = user.get('user');
    session.setUser(user);
    session.save();
    return user;
  }
}


exports.NotLoggedIn = NotLoggedIn;
exports.Auth = Auth;