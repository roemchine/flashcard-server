'use strict';
var Foxx = require('org/arangodb/foxx');
var FlashcardSetRevisionModel = require('../models/flashcard_set_revision').Model;

class FlashcardSetRevisionsRepository extends Foxx.Repository {

  getRevisions(flashcardSetId, skip, limit) {
    var aql = `
      FOR fsr IN @@collectionFlashcardSetRevisions
        FOR u IN _users
          FILTER fsr.flashcardSet == @flashcardSetId && fsr.user == u._id
          SORT fsr.createdAt DESC
          LIMIT @skip, @limit
          RETURN MERGE(fsr, {
            userEmail: u.user
          })`;

    var query = Foxx.createQuery({
      query: aql,
      model: FlashcardSetRevisionModel
    });

    return query({
      '@collectionFlashcardSetRevisions': applicationContext.collectionName('FlashcardSetRevisions'),
      flashcardSetId: flashcardSetId,
      skip: skip,
      limit: limit
    });

  }

  getRevisionsLength(flashcardSetId) {
    var aql = `
      RETURN LENGTH(
        FOR fsr2 IN @@collectionFlashcardSetRevisions
          FILTER fsr2.flashcardSet == @flashcardSetId
          RETURN fsr2
      )`;

    var query = Foxx.createQuery({
      query: aql
    });

    return query({
      '@collectionFlashcardSetRevisions': applicationContext.collectionName('FlashcardSetRevisions'),
      flashcardSetId: flashcardSetId
    })[0];
  }

  createRevision(flashcardSetId, userId) {
    var query = Foxx.createQuery(aqlQuery `LET flashcardSets = (
        FOR fs in @@collectionFlashcardSets 
          FILTER fs._id == @flashcardSetId 
          LIMIT 1 
          RETURN MERGE( 
            fs, 
            { 
              "flashcards": ( 
                FOR f IN @@collectionFlashcards 
                  FILTER f.flashcardSet == fs._id 
                  SORT f.createdAt ASC 
                  RETURN f 
              )
            }
          )
        )
        INSERT { 
          user: @userId,
          flashcardSet: @flashcardSetId,
          flashcardSetRevision: flashcardSets[0],
          createdAt: @createdAt
      } IN @@collectionFlashcardSetRevisions`);

    query({
      '@collectionFlashcardSets': applicationContext.collectionName('FlashcardSets'),
      '@collectionFlashcardSetRevisions': applicationContext.collectionName('FlashcardSetRevisions'),
      '@collectionFlashcards': applicationContext.collectionName('Flashcards'),
      userId: userId,
      flashcardSetId: flashcardSetId,
      createdAt: new Date()
    });
  }
}

exports.Repository = FlashcardSetRevisionsRepository;