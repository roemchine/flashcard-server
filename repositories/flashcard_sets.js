'use strict';
var Foxx = require('org/arangodb/foxx');
var FlashcardSetModel = require('../models/flashcard_set').Model;

class FlashcardSetsRepository extends Foxx.Repository {
  getAllFlashcardSets(skip, limit) {
    var aql = `
      FOR fs IN @@collection
        SORT fs.createdAt 
        LIMIT @skip, @limit  
        RETURN fs`
    var query = Foxx.createQuery({
      query: aql,
      model: FlashcardSetModel
    });
    var result = query({
      '@collection': applicationContext.collectionName('FlashcardSets'),
      skip: skip,
      limit: limit
    });

    return result;
  }

  getFlashcardSetsLength() {
    var aql = `
      RETURN LENGTH(
        FOR fs IN @@collectionFlashcardSets
          RETURN fs
      )`;

    var query = Foxx.createQuery({
      query: aql
    });

    return query({
      '@collectionFlashcardSets': applicationContext.collectionName('FlashcardSets')
    })[0];
  }

  getFlashcardSetWithFlashcards(_key) {
    var query = Foxx.createQuery('FOR fs IN @@collectionFs ' 
      + ' FILTER fs._key == @key '
      + ' LIMIT 1 '
      + ' RETURN merge( ' 
      + '  fs, ' 
      + '  { ' 
      + '   "flashcards": ( ' 
      + '    FOR f IN @@collectionF ' 
      + '     FILTER f.flashcardSet == fs._id '
      + '     SORT f.createdAt ASC ' 
      + '     RETURN f ' 
      + '   ) ' 
      + '  } ' 
      + ' ) ');

    var result = query({
      '@collectionFs': applicationContext.collectionName('FlashcardSets'),
      '@collectionF': applicationContext.collectionName('Flashcards'),
      'key': _key
    });

    return result;
  }

  getFlashcardSetWithFlashcardsAndCompartmentsForUser(flashcardSetKey, userId) {
    var query = Foxx.createQuery('FOR fs IN @@collectionFs ' 
      + ' FILTER fs._key == @flashcardSetKey '
      + ' LIMIT 1 '
      + ' RETURN merge( ' 
      + '   fs, ' 
      + '   { ' 
      + '     "flashcards": ( ' 
      + '       FOR f IN @@collectionF ' 
      + '         FILTER f.flashcardSet == fs._id '
      + '         SORT f.createdAt ASC ' 
      + '         RETURN f ' 
      + '     ) ' 
      + '   }, '
      + '   { '
      + '     "flashcardCompartmentInfo": FLATTEN( '
      + '       FOR box IN @@collectionFb '
      + '         FILTER box.flashcardSet == fs._id && box.user == @userId '
      + '         LET compartmentResult = ( '
      + '           FOR comp in box.flashcardBoxCompartments '
      + '             RETURN LENGTH(comp.flashcards) '
      + '         )'
      + '         RETURN compartmentResult '
      + '     ) '
      + '   } ' 
      + ' ) ');

    var result = query({
      '@collectionFs': applicationContext.collectionName('FlashcardSets'),
      '@collectionF': applicationContext.collectionName('Flashcards'),
      '@collectionFb': applicationContext.collectionName('FlashcardBoxes'),
      'flashcardSetKey': flashcardSetKey,
      'userId': userId
    });

    return result;
  }

}

exports.Repository = FlashcardSetsRepository;