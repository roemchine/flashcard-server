'use strict';
var Foxx = require('org/arangodb/foxx');

class FlashcardBoxesRepository extends Foxx.Repository {
  removeFlashcardFromFlashcardBox(flashcardSetId, flashcardId) {
    var query = Foxx.createQuery('FOR box IN @@collection'
      + ' FILTER box.flashcardSet == @flashcardSetId'
      + ' LET flashcards = ('
      + '   FOR card in flashcard_Flashcards'
      + '     FILTER card.flashcardSet == @flashcardSetId'
      + '     RETURN card'
      + ' )'
      + ' LET alteredList = ('
      + '   FOR num in 0..5'
      + '     LET newCompartmentSize = (num == 5 ?'
      + '       ROUND( LENGTH(flashcards) * 2 ) :'
      + '       num == 4 ?'
      + '       ROUND( ( LENGTH(flashcards) * 2 ) / 1.5 ) :'
      + '       num == 3 ?'
      + '       ROUND( ( LENGTH(flashcards) * 2 ) / 1.5 / 1.5 ) :'
      + '       num == 2 ?'
      + '       ROUND( ( LENGTH(flashcards) * 2 ) / 1.5 / 1.5 / 1.5 ) :'
      + '       num == 1 ?'
      + '       ROUND( ( LENGTH(flashcards) * 2 ) / 1.5 / 1.5 / 1.5 / 1.5 ) :' 
      + '       9000000'
      + '     )'
      + '     LET newFlashcards = (POSITION(box.flashcardBoxCompartments[num].flashcards, @flashcardId, false) ?'
      + '       REMOVE_VALUE(box.flashcardBoxCompartments[num].flashcards, @flashcardId, 1) :'
      + '       box.flashcardBoxCompartments[num].flashcards)'
      + '   RETURN { "compartmentSize" : newCompartmentSize, "flashcards" : newFlashcards}'
      + ' )'
      + ' UPDATE box WITH { flashcardBoxCompartments: alteredList } IN @@collection');
    query({
      '@collection': applicationContext.collectionName('FlashcardBoxes'),
      flashcardSetId: flashcardSetId,
      flashcardId: flashcardId
    });
  }

  addNewFlashcardToFlashcardBoxes(flashcardSetId, flashcardId) {
    var query = Foxx.createQuery('FOR box IN @@collection'
      + ' FILTER box.flashcardSet == @flashcardSetId'
      + ' LET flashcards = ('
      + '   FOR card in flashcard_Flashcards'
      + '     FILTER card.flashcardSet == @flashcardSetId'
      + '     RETURN card'
      + ' )'
      + ' LET alteredList = ('
      + '   FOR num in 0..5'
      + '     LET newCompartmentSize = (num == 5 ?'
      + '       ROUND( LENGTH(flashcards) * 2 ) :'
      + '       num == 4 ?'
      + '       ROUND( ( LENGTH(flashcards) * 2 ) / 1.5 ) :'
      + '       num == 3 ?'
      + '       ROUND( ( LENGTH(flashcards) * 2 ) / 1.5 / 1.5 ) :'
      + '       num == 2 ?'
      + '       ROUND( ( LENGTH(flashcards) * 2 ) / 1.5 / 1.5 / 1.5 ) :'
      + '       num == 1 ?'
      + '       ROUND( ( LENGTH(flashcards) * 2 ) / 1.5 / 1.5 / 1.5 / 1.5 ) :' 
      + '       9000000'
      + '     )'
      + '     LET newFlashcards = (num == 0 ?'
      + '       PUSH(box.flashcardBoxCompartments[num].flashcards, @flashcardId, true) :'
      + '       box.flashcardBoxCompartments[num].flashcards)'
      + '   RETURN { "compartmentSize" : newCompartmentSize, "flashcards" : newFlashcards}'
      + ' )'
      + ' UPDATE box WITH { flashcardBoxCompartments: alteredList } IN @@collection');
    query({
      '@collection': applicationContext.collectionName('FlashcardBoxes'),
      flashcardSetId: flashcardSetId,
      flashcardId: flashcardId
    });
  }
}

exports.Repository = FlashcardBoxesRepository;
